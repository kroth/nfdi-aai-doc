{% include 'logo.md' %}

# Presentations

<!-- New presentations go on top, please -->

## Infoshare 4.6.2024 13:00

- Begrüßung (Thorsten Michels)
    [slides](presentations/2406/Start_and_end_mod_wp.pptx)
- Einführung in IAM und AAI (Wolfgang Pempe und Thorsten Michels)
    [slides](presentations/2406/IAM.pptx)
- NFDI AAI Architektur  (Wolfgang Pempe und Thorsten Michels)
    [slides](presentations/2406/federations_and_nfdi-aai.pdf)
- Verschiedene AAI Lösungen 
    - didmos (Peter Gietz)
        [slides](presentations/2406/CIAM-didmos-v3.pdf)
    - RegApp (Matthias Bonn)
        [slides](presentations/2406/NFDI_CAAI-Workshop2_RegApp.pdf)
    - AcademicID (Christof Pohl)
        [slides](presentations/2406/AcademicID_Short.pdf)
    - Unity (Laura Hofer)
        [slides](presentations/2406/unity_nfdi_infoshare2.pdf)
- Betriebskonzept  (Peter Gietz)
    [slides](presentations/2406/WP4-operations.pdf)
- Incubator-Prozess  (Marcel Nellesen)
    [slides](presentations/2406/2024-06-04_IAM4NDI-Inkubatorprojekte.pdf)
- Beispiel Incubator  (Ralf Klammer)
    [slides](presentations/2406/IAM4NFDI_Incubator_NFDI4Earth_OpenedX_WorkshopDemo_20240604.pdf)
- Policy Konzept  (Wolfgang Pempe)
    [slides](presentations/2406/policies.pdf)


# Documents

Proposals of the IAM4NFDI Project:
- [**Integration** Phase](documents/iam4nfdi_integration.pdf)
- [**Initialisation** Phase](documents/iam4nfdi_initialization.pdf)

We publish the material of our workshops on this [google
drive](https://drive.google.com/drive/folders/1nT0YpZfOZ9YxQ-qgsA16zZw5KJ0TnTgV).

{% include 'gitinfo.md' %}
