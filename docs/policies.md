{% include 'logo.md' %}

# Policies and Templates

Our policy table gives an overview who needs to read or write which policy

[![policy_table](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.3/raw/images/tabelle_policy-framework.png?job=build-docs){: style="height:280px"}](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.3/raw/images/tabelle_policy-framework.png?job=build-docs)


## NFDI Policies

<small>( Current version: 0.9 )</small>

These Policies are defined by NFDI. All members must follow these
policies:

- Top Level Policy: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/00_Top-Level-Policy.pdf?job=build-docs)
- VO Membership Management Policy (VOMMP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/01_CAAI-VOMMP.pdf?job=build-docs)
- Security Incident Response Proecedure (SIRP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/02_SIRP.pdf?job=build-docs)
- Policy on Processing of Personal Data (PPPD): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/03_PPPD.pdf?job=build-docs)
- Infrastructure Attribute Policy (IAP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/04_IAP.pdf?job=build-docs)


## NFDI Policy Templates

<small>( Current version: 0.9.4 )</small>

These templates are defined by NFDI. They MAY be used as a template for
defining own policies. (In general these templates make it safer to follow
existing regulations than self-written policies.)

The documents can be edited with Libreoffice or compatible Software.

- Community AAI Acceptable Use Policy (CAAI-AUP):
    (english: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-CAAI_AUP.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template.docx?job=build-docs)),
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-CAAI_AUP_DE.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template_DE.docx?job=build-docs))
- VO Acceptable Use Policy (VO-AUP):
    english: ([pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template.docx?job=build-docs)),
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template_DE.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template_DE.docx?job=build-docs))
- Service Acceptable Use Policy (SAUP):
    english: ([pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template.docx?job=build-docs)),
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template_DE.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template_DE.docx?job=build-docs))
- Service Access Policy (SAP):
    english: ([pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/06_SAP_template.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/06_SAP_template.docx?job=build-docs))
- VO Lifecycle Policy:
    english: ([pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/91_VO-lifecycle.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/91_VO-lifecycle.docx?job=build-docs))
- Pricay Statement Template:
    english: ([pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template.docx?job=build-docs)),
    (german: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template_DE.pdf?job=build-docs)
    [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template_DE.docx?job=build-docs))
- Template Verfahrensverzeichnis:
    [odt](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/Template_Verfahrensverzeichnis_AAI.odt?job=build-docs)


## Underlying Policy Frameworks

The following Policy Frameworks / Recommendations are intended to be supported:

- [Security Incident Response Trust Framework for Federated Identity (Sirtfi)](https://refeds.org/sirtfi)
- [AARC Guidelines Documents Approved by AEGIS](https://wiki.geant.org/display/AARC/AARC+Documents+Approved+by+AEGIS)
- [REFEDS Data Protection Code of Conduct 2.0 (DPCoCo2)](https://refeds.org/category/code-of-conduct)


{% include 'gitinfo.md' %}
