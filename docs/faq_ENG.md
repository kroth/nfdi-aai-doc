{% include 'logo.md' %}

# Frequently Asked Questions

## NFDI AAI


<details>
<summary>

Is the NFDI AAI only compatible with the four Community
AAI products mentioned?

</summary>

The NFDI AAI is basically structured in such a way that the largest
possible number of different components can be used. This is particularly
true for Community AAI products. If the attributes and policies correspond
to the AEGIS-Endorsed Recommendations, they can be integrated as Community
AAIs. We cannot maintain a list of growing products at this point,
especially since some products support different attribute profiles.
Compatibility depends crucially on this. A good starting point is support
of the standards SAML2 and OIDC. For details see
(<a href="https://doc.nfdi-aai.de/">documentation</a>). We also have a <a href="https://docs.google.com/spreadsheets/d/1EHUo3KS4KDaQZomR5c1LYK1fQA2_4ZjsIx9xlEXrKac/edit#gid=0">feature comparison matrix here</a>

It is important that 1 consortia should pick exactly one of the four solutions that is then applicable to all their services!

</details>


<details>
<summary> Can the "NFDI AAI" query attributes from multiple community AAIs? </summary>

In principle yes, but this should only be used if collecting attributes from multiple AAIs is nesseccary.
</details>

## Community AAI

#### Functionality and Technical Implementation

<details> <summary> Do the four Community AAIs  already exist or will they be set up again as a separate instance? </summary>  

This depends on each instance operator individually. The software exist and at least one instance is run by the respective experts. If the solution supports tenants, such an instance can be used by multiple consortia. It would also be possible to run an instance exclusively for
one consortium, but due to limited ressources this is not available for free. This means that all additional costs must be paid by the consortia themselves. 
</details>

<details>
<summary>What are the differences between the four Community AAIs? </summary>

All four AAI solutions are based on the AARC Blueprint Architecture. Unfortunately, a comparison table is quite political and is still subject to constant change. Therefore we offer a feature table where each community AAI solution specifies their features. Each consortia can also specify “No preference” there. We then consider which Community AAI solution best suits to the respective NFDI consortium. Unless there is a particular feature of interest for you, any of the solutions can be choosen.
</details>

<details> <summary>Does each of the four Community AAIs provide a different identity to the service for login?</summary>  

In principle yes, but “different” is a bit too general. In general, each consotiom has one AAI based on one of the four solutions. An identity is described with the same set of attributes, the values of the attributes can be different. It is possible to link different identities of the same user together. This can either be done centrally in the Infra Proxy or decentrally in the application.
</details>


<details> <summary>Is there an automatic mechanism for dealing with different identities?</summary> 

No, duplicate avoidance (e.g. ID matching) is not intended in the project. In general, each consortia has one AAI based on one of the four solutions which is why this should generally not happen. In principle, it is possible for a user to have multiple accounts if a certain user has different accounts from different consortia. In general, managing these accounts manuelly is possible, but is left to the services or users respectively.
</details>

<details> <summary>Is information shared between the four Community
AAIs?</summary>

No, an exchange at this level is not planned. Therefore, consortia must
choose a solution and organize themselves only there.

</details>

#### Regulations and Requirements


<details>
<summary>Do the Community AAIs provide High Availability (HA) and Service Level Agreement (SLA)?</summary>

Yes, this is possible for all four AAIs, but it depends on implementation if it is include in free instances or chargeable ones.
</details>


<details>
<summary>Is there a backup in case of an emergency?</summary>

Yes, all consortia have backups.
</details>


<details>
<summary>How is data security ensured in the solution?</summary>

Questions regarding data security can be found in the provided operating concept. There will also be demo instances such that you (the NFDI consortia) can form a basis for deciding on a solution.

</details>

#### Responsibilities and Organizational Issues

<details>
<summary>Who takes care of the setup, configuration and operation? </summary>

As a part of the <a href="documents/iam4nfdi_initialization.pdf">[just requested]</a> B4N/IAM projects, the four solutions are operated as part of the IAM project.
</details>


<details>
<summary> Who makes the decision for a software solution for the AAI community </summary>

A community can be seen as well-organized unit which is typically an NFDI consortium.
</details>

## Service Connection

#### General

<details>
<summary>Where should a virtual organization (VO) that regulates access to ELN/Repo be inserted?
</summary>

The VOs are maintained in the respective community AAI. That is main purpose.
</details>

<details>
<summary>Can a service use attributes from home organization? </summary>

Yes. It is intended to forward selected attributes from the home organization to the service. This is intended to enable the service, for example, to use the status of a user (student, staff, ...) or permissions via "entitlements". The “assurance” is also forwarded by the home organization (if they provide it).
</details>

<details>
<summary>What does a service have to consider regarding to service connection?</summary>

This depends on the number of consortia that are to be connected to the service. If only one consortia have to be considered this should be clarified be the consortia itself. If more than one consortia is involved the service has to be consider the NFDI proxy and should talk with the NFDI Team.
</details>

#### Roles, Groups and Users

<details>
<summary>Where are the rights regarding roles/groups or users to specific data sets maintained?</summary>

In the Community AAI.
</details>

<details>
<summary>Do users have a set of linked IdPs to identities from which attributes can be requested? </summary>

No. Services do not request the attributes directly from the IdPs, but receive them exclusively via the proxy to which they are connected. Of course, services are free to collect additional attributes. This is not intended in the architecture and cannot be supported for the time being.
</details>

<details>
<summary>How to deal with external users?</summary>

All users from universities can use the login from their home institution via DFN-AAI or eduGAIN. All CAAI softwares have solutions to include other users, e.g. via ORCID or "social" logins. Also from 2025 on all users can use an DFN-Edu-ID-Login <a href="https://www.dfn.de/eine-fuer-alle-die-edu-id/">(see here)</a>.

</details>


#### Support Model:

<details>
<summary>
How will support look like for IAM4NFDI?
</summary>

We intend to provide the following support:
- Level 1 - Community admins (training for support required)
- Level 2 - IAM project team
- after the end of the project, support will be provided by the Community
    AAI providers (to be contracted with the individual provider) 
- operations and support for the central components can be provided, e.g.
    by the DFN (operating model yet to be defined)

</details>


<details>
<summary>
Is the usage of a CAAI free of charge?	
</summary>

Usage of a central CAAI instance is free of charge. However, if the
functionality needs to be adapted to specific requirements and an
individual CAAI instance is needed, this will come with associated costs.
Incubators offer a framework to implement additional functionality in the
CENTRAL components or the CAAIs. This is free of charge but limited up to
a certain level of complexity.

</details>

{% include 'gitinfo.md' %}
